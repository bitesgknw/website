---
title: "A proposito di.." 
layout: "about" 
description: "Di che si tratta?" 
aliases: ["about-us"] 
author: "Michela" 
output: html_document
---


```{r setup, include=FALSE}
library(knitr)
library(cowplot)
library(rgdal)
library(leaflet)
library(viridis)
library(magick)
library(pdftools)
knitr::opts_chunk$set(collapse = TRUE, fig.align = "center")
# manually set the working directory to be equal to the rproject working directory!
#knitr::opts_knit$set(root.dir = normalizePath(".."))
knitr::opts_knit$set(root.dir = rprojroot::find_rstudio_root_file())
```

```{r,  include=FALSE}
p1 <- ggdraw() + draw_image(magick::image_read("./static/img/Logo.png", density = 1000), 
                            scale = 0.6)
p2 <- ggdraw() + draw_image(magick::image_read("./static/img/michela2.png", density = 1000), 
                            scale = 0.8)
p3 <- ggdraw() + draw_image(magick::image_read("./static/img/sara.png", density = 1000), 
                            scale = 0.8)
```

```{r logo, fig.show='asis', fig.cap = "", echo = FALSE}
#*Tachyglossus aculeatus* o	Short-beaked Echidna,	Echidna dal becco corto
p1
```



Questa è una piattaforma dedicata alla divulgazione di curiosità di ecologia e ambiente. Ci auguriamo di ispirare i lettori a conoscere meglio il meraviglioso mondo naturale che ci circonda e ad agire per un presente e futuro sostenibile!

Nella sezione **curiosità**, raccontiamo anedotti sulle meraviglie della natura. Nella sezione **scienza per tutti**, presentiamo scoperte scientifiche. Nella sezione **sostenibilità**, forniamo informazioni utili su come ridurre il nostro impatto sul mondo naturale. Nella sezione **schizzi**, collezioniamo i nostri disegni di creature che amiamo.


# Chi siamo?

## Michela Busana

```{r miche, fig.show='asis', fig.cap = "", echo = FALSE}
#*Tachyglossus aculeatus* o	Short-beaked Echidna,	Echidna dal becco corto
p2
```



## Sara Raj Pant

```{r sara, fig.show='asis', fig.cap = "", echo = FALSE}
#*Tachyglossus aculeatus* o	Short-beaked Echidna,	Echidna dal becco corto
p3
```

