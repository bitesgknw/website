# website

To render the website install the package blogdwon in Rstudio, open the file script.R (in the repo) and run it! 

## Including images

Pics should have a resolution of max:
*  1920  y pixel max
*  quality 0.7  passed on to jpeg::writeJPEG()

use the package magick to convert the images: see [link](https://github.com/rstudio/blogdown/issues/51)

```r
library(magick)

logo <- image_read('./static/img/IMG_8168.JPG')
logo <- image_scale(logo, "x400")
image_write(logo, path = "./static/img/logo.png", format = "png")
```

Or simply use directly the program magick. See [example](https://stackoverflow.com/questions/7261855/recommendation-for-compressing-jpg-files-with-imagemagick), for example in Ubuntu 18:

```
convert -strip -interlace Plane -gaussian-blur 0.05 -quality 65% IMG_8474.JPG result.jpg
```
where IMG_8474.JPG is the original, result.jpg the output, and 65 the quality/size of the result.


